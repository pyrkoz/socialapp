export interface FuelReportView {
    month: string,
    averageConsumption: number
}